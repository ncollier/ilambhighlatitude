High Latitiude ILAMB Plugin
===========================

The following code and data constitutes a plugin to the
`ILAMB <https://bitbucket.org/ncollier/ilamb>`_ benchmarking system
which targets model performance at high latitudes. To compare model
output using this configure file you will need to:

* Install the ILAMB package
* Add the contents of the included ``DATA`` directory to the data in your
  ``ILAMB_ROOT``
* Copy the driver script from the ILAMB code directory to this location

  cp PATH_TO_ILAMB_CODEBASE/demo/driver.py .

* Run the driver on the included configure file

  python driver.py --config hilat.cfg --model_root PATH_TO_MODELS --regions arctic global
  
